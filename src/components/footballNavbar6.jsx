/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
class NavBar extends Component {
  render() {
    let { Count, } = this.props;

    return ( 
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
          <a className="navbar-brand" href="#">
            Football Tournament
          </a>
          <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Number of Matches
                  <span className="badge badge-pill bg-primary mx-2 badge-secondary">
                    {Count}
                  </span>
                </a>
              </li> 

             

            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}
export default NavBar;
