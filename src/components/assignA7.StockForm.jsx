import React, { Component } from "react";
class StockForm extends Component {
  state = {
    stock1: this.props.stock1,
    codeArr: this.props.codeArr,
  };
  makeDropdown = (arr, value, name, label) => {
    return (
      <div className="form-group my-1">
        <select
          className="form-control"
          name={name}
          value={value}
          onChange={this.handleChange}
        >
          <option value="">{label}</option>
          {arr.map((opt) => (
            <option>{opt}</option>
          ))}
        </select>
      </div>
    );
  };

  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    s1.stock1[input.name] = input.value;
    this.setState(s1);
  };

  handleToHomePage = () => {
    this.props.onHomePage();
  };
  handleSubmitStock = (e) => {
    e.preventDefault();
    this.props.onSubmitStock(this.state.stock1);
  };
  getAllDateOfMonth = (mArr, year, month) => {
    let arr = [];
    var Month = mArr.findIndex((m) => m === month) + 1;
    var days =year? new Date(year, Month, 0).getDate():0;
    for (var d = 1; d <= days; d++) {
      arr.push(d);
    }
    return arr;
  };

  render() {
    let { stock1, codeArr } = this.state;
    let { code = "", quantity = "", year = "", month = "", date = "" } = stock1;
    let yearArr = [];
    for (let i = 1900; i <= 2025; i++) {
      yearArr.push(i);
    }
    let monthArr = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let dateArr = this.getAllDateOfMonth(monthArr, stock1.year, stock1.month);
    // console.log(yearArr);
    return (
      <React.Fragment>
        {this.makeDropdown(codeArr, code, "code", "Select Code")}
        <div class="form-group">
          <label>Stock Received</label>
          <input
            type="number"
            class="form-control"
            id="quantity"
            name="quantity"
            value={quantity}
            placeholder="Enter Product Price"
            onChange={this.handleChange}
          />
        </div>
        <div className="row">
          <div className="col-4">
            {this.makeDropdown(yearArr, year, "year", "Select Year")}
          </div>
          <div className="col-4">
            {" "}
            {this.makeDropdown(monthArr, month, "month", "Select Month")}
          </div>
          <div className="col-4">
            {" "}
            {this.makeDropdown(dateArr, date, "date", "Select Date")}
          </div>
        </div>
        <button className="btn btn-primary" onClick={this.handleSubmitStock}>
          Submit
        </button>
        <br />
        <button
          className="btn btn-primary my-4"
          onClick={this.handleToHomePage}
        >
          Go To Home Page
        </button>
      </React.Fragment>
    );
  }
}
export default StockForm;
