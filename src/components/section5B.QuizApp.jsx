import React, { Component } from "react";
import { getQuizQuestions } from './section5B.QuizQuestions'
class QuizeGame extends Component {
  state = {
    players: [
      { name: "James", points: 0 },
      { name: "Julia", points: 0 },
      { name: "Martha", points: 0 },
      { name: "Steve", points: 0 },
    ],
    questions: getQuizQuestions(),
    currentQuestion: 0,
    playerBuzzer: null,
  };
  handleBuzzer = (idx) => {
    let s1 = { ...this.state };
    s1.playerBuzzer = s1.players[idx].name;
    this.setState(s1);
  };

  handleCurrectAnswer = (ans) => {
    let s1 = { ...this.state };
    let plyr = s1.players.find((p1) => p1.name === s1.playerBuzzer);
    console.log(s1.currentQuestion);
    if (plyr) {
      if (s1.questions[s1.currentQuestion].answer === ans) {
        plyr.points = plyr.points + 3;
        window.alert("Correct Answer. You get 3 points");
        // s1.currentQuestion++;
      } else {
        plyr.points = plyr.points - 1;
        window.alert("Worng Answer. You lose 1 points");
      }

      s1.currentQuestion =
        s1.currentQuestion === s1.questions.length
          ? s1.currentQuestion 
          : s1.currentQuestion + 1;
          console.log(s1.currentQuestion);
    }
    console.log(s1.currentQuestion);
    s1.playerBuzzer = null;
    this.setState(s1);
    console.log(s1.currentQuestion);
  };

  render() {
    let { players, questions, currentQuestion, playerBuzzer } = this.state;
    // let Options = questions[currentQuestion].options;
    let higstScore = players.reduce((p1, p2) =>
      p1.points > p2.points ? p1 : p2
    );
    let checkWinner = players.filter((p1) => p1.points === higstScore.points);
    console.log("R", currentQuestion);
    return (
      <React.Fragment>
        <div className="container">
          <h4 className="text-center display-3">Welcome to Quiz Contest</h4>
          <hr />
          <p className="text-center ">
            <b>Participants </b>
          </p>

          <div className="row text-center m-3">
            {players.map((p1, index) => (
              <div
                className={
                  playerBuzzer === p1.name
                    ? "col-2 mx-4 bg-success"
                    : "col-2 mx-4 bg-warning "
                }
              >
                <h6>Name : {p1.name} </h6>
                <p>
                  Score : <b>{p1.points}</b>{" "}
                </p>
                <button
                  className="btn bg-white text-black m-2 btn-sm"
                  onClick={() => this.handleBuzzer(index)}
                >
                  BUZZER
                </button>
              </div>
            ))}
          </div>
          <div className="text-center">
            {currentQuestion=== questions.length  ? (
              <>
                <h4 className="text-center">Game Over</h4>
                <h5>
                  {checkWinner.length > 1
                    ? "There is a tie. The winners are  " +
                      checkWinner.map((n1) => n1.name).join(", ")
                    : "The winner is " + checkWinner[0].name}
                </h5>
              </>
            ) : (
              <>
                <h2 className="text-center">
                  Question Number : {currentQuestion + 1}{" "}
                </h2>
                <h4> {questions[currentQuestion].text} </h4>
                {questions[currentQuestion].options.map((opt1, index) => {
                  return (
                    <button
                      className="btn btn-primary text-white m-2 btn-sm"
                      onClick={() => this.handleCurrectAnswer(index + 1)}
                    >
                      {opt1}
                    </button>
                  );
                })}
              </>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default QuizeGame;
