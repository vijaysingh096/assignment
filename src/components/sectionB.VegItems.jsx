/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import Cart from "./sectionB.Cart";

class VegItems extends Component {
  state = {
    cart: this.props.cart,
    vegPizzaArr: this.props.vegPizzaArr,
    sizesArr: this.props.sizesArr,
    crustsArr: this.props.crustsArr,
    ddSelVal: new Array(this.props.vegPizzaArr.length).fill(""),
    dd1: { size: "", crust: "" },
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    s1.dd1[input.name] = input.value;
    s1.ddSelVal[input.id] = s1.dd1;
    this.setState(s1);
    // console.log(s1.dd1);
  };
  addVegToCart = (index, itemName) => {
    let s1 = { ...this.state };
    let siz = s1.ddSelVal[index].size;
    let cru = s1.ddSelVal[index].crust;
    console.log(siz, cru, itemName);

    let d1 = s1.cart.find((dish) => dish.name === itemName);
    if (d1) {
      d1.quntity++;
    } else {
      let item1 = s1.vegPizzaArr.find((ele) => ele.name === itemName);
      let newArr = {
        image: item1.image,
        name: item1.name,
        desc: item1.desc,
        size: siz,
        crust: cru,
        quntity: 0,
      };
      newArr.quntity = 1;
      if (!siz) {
        return window.alert("select size");
      } else if (!cru) {
        return window.alert("select curst");
      } else {
        this.props.addCart(newArr);

        // s1.cart.push(newArr);
        s1.dd1 = { size: "", crust: "" };
      }
    }

    this.setState(s1);
  };

  handleAdd = (Name) => {
    this.props.addOne(Name);
  };
  handleSubt = (Name) => {
    this.props.subtOne(Name);
  };
  render() {
    let { cart, vegPizzaArr, sizesArr, crustsArr, ddSelVal } = this.state;

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-8">
            <div className="row">
              {vegPizzaArr.map((pizza1, index) => {
                let { image, name, desc } = pizza1;
                let cartItem = cart.find((item) => item.name === name);

                return (
                  <div className="col-6">
                    <div
                      className="card text-center"
                      style={{ width: 90 + "%" }}
                    >
                      <img
                        className="card-img-top align-items-center "
                        style={{ width: 90 + "%" }}
                        src={image}
                        alt="Card image cap"
                      />
                      <div className="card-body">
                        <h5 className="card-title text-center"> {name}</h5>
                        <p className="card-text">{desc}</p>
                        {cartItem ? (
                          <>
                            <div className="row">
                              {" "}
                              <div className="col-6">
                                {this.makeDropdown(
                                  sizesArr,
                                  index,
                                  cartItem.size,
                                  "size",
                                  "Select Size",
                                  cartItem.quntity > 0 ? true : false
                                )}{" "}
                              </div>{" "}
                              <div className="col-6">
                                {" "}
                                {this.makeDropdown(
                                  crustsArr,
                                  index,
                                  cartItem.crust,
                                  "curst",
                                  "Select Crust",
                                  cartItem.quntity > 0 ? true : false
                                )}
                              </div>
                            </div>

                            <br />
                            <button
                              className="btn btn-danger btn-sm"
                              onClick={() => this.handleSubt(cartItem.name)}
                            >
                              -
                            </button>
                            <button
                              className="btn btn-secondary mx-2 btn-sm"
                              disabled={true}
                            >
                              {cartItem.quntity}
                            </button>
                            <button
                              className="btn btn-success btn-sm "
                              onClick={() => this.handleAdd(cartItem.name)}
                            >
                              +
                            </button>
                          </>
                        ) : (
                          <>
                            <div className="row">
                              {" "}
                              <div className="col-6">
                                {this.makeDropdown(
                                  sizesArr,
                                  index,
                                  ddSelVal[index] ? ddSelVal[index].size : "",
                                  "size",
                                  "Select Size",
                                  false
                                )}
                              </div>{" "}
                              <div className="col-6">
                                {" "}
                                {this.makeDropdown(
                                  crustsArr,
                                  index,
                                  ddSelVal[index] ? ddSelVal[index].crust : "",
                                  "crust",
                                  "Select Crust",
                                  false
                                )}
                              </div>
                            </div>

                            <br />
                            <button
                              className="btn btn-primary "
                              onClick={() => this.addVegToCart(index, name)}
                            >
                              Add To Cart
                            </button>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="col-4 border">
            <Cart
              cart={cart}
              addOne={this.props.addOne}
              subtOne={this.props.subtOne}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
  makeDropdown = (arr, id, value = "", name, label, disable = false) => {
    return (
      <div className="form-group my-1">
        <select
          className="form-control"
          id={id}
          name={name}
          value={value}
          disabled={disable}
          onChange={this.handleChange}
        >
          <option value="">{label}</option>
          {arr.map((opt, index) => {
            return <option key={index}>{opt}</option>;
          })}
        </select>
      </div>
    );
  };
}
export default VegItems;
