/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Link } from "react-router-dom";
class NavBarB extends Component {
  render() {
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-light ">
          <Link className="navbar-brand" to="/">
            MYFavPizza
          </Link>
          <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item" >
                <Link className="nav-link" to="/vegpizza">
                  Veg Pizza
                </Link>
              </li>
              <li className="nav-item" >
                <Link className="nav-link" to="/nonvegpizza">
                  Non-Veg Pizza
                </Link>
              </li>
              <li className="nav-item" >
                <Link className="nav-link" to="/sidedishes">
                  Side Dishes
                </Link>
              </li>
              <li className="nav-item" >
                <Link className="nav-link" to="/otheritem">
                  Other Items
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}
export default NavBarB;
