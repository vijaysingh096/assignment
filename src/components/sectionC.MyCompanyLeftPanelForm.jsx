import React, { Component } from "react";
class LeftPanelForm extends Component {
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let options = { ...this.props.options };
    let {loc}=this.props
    input.type === "checkbox"
      ? (options[input.name] = this.updateCBs(
          options[input.name],
          input.checked,
          input.value
        ))
      : (options[input.name] = input.value);
    this.props.onOptionChange(loc,options);
  };

  updateCBs = (inpValue, checked, value) => {
    let inpArr = inpValue ? inpValue.split(",") : [];
    if (checked) inpArr.push(value);
    else {
      let index = inpArr.findIndex((ele) => ele === value);
      if (index >= 0) inpArr.splice(index, 1);
    }
    return inpArr.join(",");
  };
  makeCheckboxes = (arr, values, name, lable) => {
    return (
      <React.Fragment>
        <lable className="form-check-lable font-weight-bold">
          <b> {lable}</b>{" "}
        </lable>
        {arr.map((opt, index) => (
          <div className="form-check" key={index}>
            <input
              className="form-check-input"
              value={opt}
              type="checkbox"
              name={name}
              checked={values.find((val) => val === opt) || false}
              onChange={this.handleChange}
            />
            <lable className="form-check-lable">{opt} </lable>
          </div>
        ))}
      </React.Fragment>
    );
  };
  makeRadios = (arr, values, name, lable) => {
    return (
      <React.Fragment>
        <lable className="form-check-lable font-weight-bold">
          <b> {lable}</b>{" "}
        </lable>
        {arr.map((opt, index) => (
          <div className="form-check" key={index}>
            <input
              className="form-check-input"
              value={opt}
              type="radio"
              name={name}
              checked={values === opt || false}
              onChange={this.handleChange}
            />
            <lable className="form-check-lable">{opt} </lable>
          </div>
        ))}
      </React.Fragment>
    );
  };
  render() {
    let { department = "", designation = "" } = this.props.options;
    let { allOptions } = this.props;

    return (
      <React.Fragment>
        <div className="container">
          <div className="row border bg-light">
            <div className="col-12">
              {this.makeRadios(
                allOptions.designation,
                designation,
                "designation",
                "Select Designation"
              )}
            </div>
            <div className="col-12">
              {this.makeCheckboxes(
                allOptions.department,
                department.split(","),
                "department",
                "Select Department"
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanelForm;
