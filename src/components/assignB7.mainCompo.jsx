import React, { Component } from "react";
class MainCompoAssign7B extends Component {
  state = {
    prodArr: [
      { code: "A101", price: 150 },
      { code: "A452", price: 450 },
      { code: "B671", price: 52 },
      { code: "H887", price: 17 },
      { code: "V693", price: 188 },
      { code: "A645", price: 306 },
      { code: "J034", price: 109 },
      { code: "N299", price: 75 },
      { code: "M472", price: 250 },
      { code: "R077", price: 300 },
      { code: "B297", price: 150 },
      { code: "A489", price: 160 },
      { code: "A507", price: 25 },
      { code: "K563", price: 45 },
      { code: "M833", price: 80 },
      { code: "T672", price: 100 },
      { code: "B934", price: 200 },
    ],
    strVal: "",
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    s1.strVal = input.value;
    this.setState(s1);
  };
  handleShowArr = (arr, str) => {
    console.log(str);
    if (!str) return arr;
    if (str.indexOf("<") === 0) {
      let newStr = str.replace("<", "");
      newStr.includes(">" || "<")
        ? window.alert("invalid input") 
        : (arr = arr.filter((val) => val.price < parseInt(newStr)));
      console.log(newStr);
    }
    if (str.indexOf(">") === 0) {
      let newStr = str.replace(">", "");
      newStr.includes(">" || "<")
        ? window.alert("invalid input")
        : (arr = arr.filter((val) => val.price > parseInt(newStr)));
    }
    if (str.includes("-")) {
      let arr1 = str.split("-");

      arr1.length === 2
        ? (arr = arr.filter(
            (val) =>
              val.price >= parseInt(arr1[0]) && val.price <= parseInt(arr1[1])
          ))
        : window.alert("invalid input");
      console.log(arr1);
    }
    return arr;
  };
  render() {
    let { prodArr, strVal } = this.state;
    let showData = this.handleShowArr(prodArr, strVal);
    return (
      <React.Fragment>
        <div className="container ">
          <div class="form-group m-2">
            <label>Enter Product Value </label>
            <input
              type="text"
              class="form-control"
              id="strVal"
              name="strVal"
              value={strVal}
              placeholder="eg. <100 or >100 or 10-30 value"
              onChange={this.handleChange}
            />
          </div>
          <div className="row border bg-dark text-white text-center">
            <div className="col-6">Code</div>
            <div className="col-6">Price</div>
          </div>
          {showData.map((d1) => (
            <div className="row border text-center">
              <div className="col-6 border bg-light">{d1.code}</div>
              <div className="col-6 border bg-light">{d1.price}</div>
            </div>
          ))}
        </div>
      </React.Fragment>
    );
  }
}
export default MainCompoAssign7B;
