import React, { Component } from "react";
import qureyString from "query-string";
import LeftPanelForm from "./sectionC.MyCompanyLeftPanelForm";
class CompLocation extends Component {
 
  handleOptionChange = (Loc, Opt) => {
      Opt.page=1
    Loc ? this.callURL(`/emps/${Loc}`, Opt) : this.callURL("/emps", Opt);
  };
  callURL = (url, Options) => {
    let searchString = this.makeSearchString(Options);
    this.props.history.push({
      pathname: url,
      search: searchString,
    });
  };
  filterParams = (arr, qParam) => {
    let { department, designation } = qParam;
    arr = this.filterParamsOneByOne(arr, "department", department);
    arr = this.filterParamsOneByOne(arr, "designation", designation);

    return arr;
  };
  filterParamsOneByOne = (arr, name, values) => {
    if (!values) return arr;
    let valuesArr = values.split(",");
    let newArr = arr.filter((emp1) =>
      valuesArr.find((v1) => v1 === emp1[name])
    );
    return newArr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;

  makeSearchString = (Options) => {
    let { department, designation, page } = Options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "department", department);
    searchStr = this.addToQueryString(searchStr, "designation", designation);
    searchStr = this.addToQueryString(searchStr, "page", page);
    return searchStr;
  };

  makeAllOptions = (arr) => {
    let json = {};
    json.department = this.getDifferentValue(arr, "department");
    json.designation = this.getDifferentValue(arr, "designation");
    return json;
  };

  getDifferentValue = (arr, name) =>
    arr.reduce(
      (acc, curr) =>
        acc.find((val) => val === curr[name]) ? acc : [...acc, curr[name]],
      []
    );

  handlePageChange = (txt, cPage) => {
    let { loc } = this.props.match.params;
    let options = { ...qureyString.parse(this.props.location.search) };
    options.page = txt === "Pre" ? cPage - 1 : cPage + 1;
    loc ? this.callURL(`/emps/${loc}`, options) : this.callURL("/emps", options);
  };
  render() {
    let { loc } = this.props.match.params;
    let { employees } = this.props;
    let qureyParams = qureyString.parse(this.props.location.search);
    const { page = 1 } = qureyParams;
    let pageNum =  +page;
    let size = 1;
    let employee1 = loc
      ? employees.filter((emp) => emp.location === loc)
      : employees;
    let employee2 = this.filterParams(employee1, qureyParams);
    console.log("qp", qureyParams);
    let startIndex = (pageNum - 1) * size;
    let endIndex =
      employee2.length > startIndex + size - 1
        ? startIndex + size - 1
        : employee2.length - 1;

    let employee3 =
      employee2.length > 2
        ? employee2.filter(
            (lt, index) => index >= startIndex && index <= endIndex+1
          )
        : employee2;
    let allOptions = this.makeAllOptions(employees);
    // console.log(allOptions);

    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-3">
              <LeftPanelForm
                allOptions={allOptions}
                options={qureyParams}
                onOptionChange={this.handleOptionChange}
                loc={loc}
              />
            </div>
            <div className="col-9">
              <div className="row">
                <div className="col-12">
                  <h2> Welcome to Employee Portal</h2>
                  <hr />
                  <b> You have choosen </b>
                  <b> Location : {loc ? loc : "All"}</b>
                  <br />
                  Department :
                  <b>
                    {qureyParams.department ? qureyParams.department : "All"}{" "}
                  </b>
                  <br />
                  Designation :
                  <b>
                    {qureyParams.designation ? qureyParams.designation : "All"}
                  </b>
                  <br /> the number of employees matching the options :
                  <b>{employee2.length} </b>
                </div>
              </div>
              <div className="row border">
                {employee3.map((emp) => (
                  <div className="col-6 bg-light border">
                    <b>{emp.name}</b>
                    <br />
                    {emp.email}
                    <br />
                    {emp.mobile}
                    <br />
                    {emp.location}
                    <br />
                    {emp.department}
                    <br />
                    {emp.designation}
                    <br />
                    {emp.salary}
                    <br />
                  </div>
                ))}
              </div>
              <div className="row my-3">
                <div className="col-2">
                  {startIndex > 0 ? (
                    <button
                      className="btn btn-primary btn-sm"
                      onClick={() => this.handlePageChange("Pre",pageNum)}
                    >
                      Pre
                    </button>
                  ) : (
                    ""
                  )}
                </div>
                <div className="col-8"></div>
                <div className="col-2">
                  {endIndex < employee2.length - 1 ? (
                    <button
                      className="btn btn-primary btn-sm"
                      onClick={() => this.handlePageChange("Next",pageNum)}
                    >
                      Next
                    </button>
                  ) : (
                    ""
                  )}{" "}
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CompLocation;
