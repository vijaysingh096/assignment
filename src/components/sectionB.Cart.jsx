/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";

class Cart extends Component {
  state = {
    cart: this.props.cart,
  };

  handleAdd = (Name) => {
    this.props.addOne(Name);
  };
  handleSubt = (Name) => {
    this.props.subtOne(Name);
  };
  render() {
    let { cart } = this.state;
    return (
      <React.Fragment>
        <h2 class="text-center">
          {cart.length > 0 ? "Cart" : "Cart is empty"}{" "}
        </h2>
        <hr />
        <h6> {cart.length} Items added in cart</h6>
        <hr />
        {cart.length > 0 ? (
          <>
            {cart.map((itm, index) => {
              let { image, name, desc, size, crust, quntity } = itm;
              return (
                <div className="row border">
                  <div className="col-4">
                    <img
                      className="img align-items-center  "
                      style={{ width: 90 + "%"  }}
                      src={image}
                      alt="Card image cap"
                    />
                  </div>
                  <div className="col-7">
                    <b > {name}</b><br/>
                    {desc ? desc: ""}<br/>
                    {crust ? <b>{size + "/" + crust} </b> : ""}
<br/>
                    <button
                      className="btn btn-danger btn-sm "
                      onClick={() => this.handleSubt(name)}
                    >
                      -
                    </button>
                    <button
                      className="btn btn-secondary mx-2 btn-sm"
                      disabled={true}
                    >
                      {" "}
                      {quntity}{" "}
                    </button>
                    <button
                      className="btn btn-success btn-sm "
                      onClick={() => this.handleAdd(name)}
                    >
                      +
                    </button>
                  </div>
                </div>
              );
            })}
          </>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  }
}
export default Cart;
