import React, { Component } from "react";
import NavBar from "./footballNavbar6";
class FootballMainCompo extends Component {
  state = {
    count: 0,
    viewIndex: -1,
    pointArr: [
      {
        name: "France",
        played: 0,
        won: 0,
        lost: 0,
        draw: 0,
        goal: 0,
        goalAgainst: 0,
        points: 0,
      },
      {
        name: "England",
        played: 0,
        won: 0,
        lost: 0,
        draw: 0,
        goal: 0,
        goalAgainst: 0,
        points: 0,
      },
      {
        name: "Brazil",
        played: 0,
        won: 0,
        lost: 0,
        draw: 0,
        goal: 0,
        goalAgainst: 0,
        points: 0,
      },
      {
        name: "Germany",
        played: 0,
        won: 0,
        lost: 0,
        draw: 0,
        goal: 0,
        goalAgainst: 0,
        points: 0,
      },
      {
        name: "Argentina",
        played: 0,
        won: 0,
        lost: 0,
        draw: 0,
        goal: 0,
        goalAgainst: 0,
        points: 0,
      },
    ],
    team1: { name: "", score: 0 },
    team2: { name: "", score: 0 },
    allMatches: [],
  };
  handleView = (num) => {
    let s1 = { ...this.state };
    s1.viewIndex = num;
    s1.team1 = { name: "", score: 0 };
    s1.team2 = { name: "", score: 0 };
    this.setState(s1);
  };

  handleTeam1 = (Name) => {
    let s1 = { ...this.state };
    s1.team1.name = Name;
    this.setState(s1);
  };

  handleTeam2 = (Name) => {
    let s1 = { ...this.state };
    s1.team2.name = Name;
    this.setState(s1);
  };
  handleStatMatch = () => {
    let s1 = { ...this.state };
    if (!s1.team1.name) {
      return window.alert("select team 1");
    } else if (!s1.team2.name) {
      return window.alert("select team 2");
    } else if (s1.team1.name === s1.team2.name) {
      return window.alert("select different teams ");
    } else {
      s1.viewIndex = 4;
    }
    this.setState(s1);
  };
  handleScore = (Name) => {
    let s1 = { ...this.state };
    s1.team1.score =
      Name && Name === s1.team1.name ? s1.team1.score + 1 : s1.team1.score;
    s1.team2.score =
      Name && Name === s1.team2.name ? s1.team2.score + 1 : s1.team2.score;
    this.setState(s1);
  };
  handleMatchOver = () => {
    let s1 = { ...this.state };
    s1.viewIndex = -1;
    let json = {};
    json.team1 = s1.team1.name;
    json.team2 = s1.team2.name;
    json.score1 = s1.team1.score;
    json.score2 = s1.team2.score;
    s1.allMatches.push(json);
    let player1 = s1.pointArr.find((p1) => p1.name === s1.team1.name);
    let player2 = s1.pointArr.find((p1) => p1.name === s1.team2.name);
    if (player1) {
      player1.played = player1.played + 1;
      player1.won =
        s1.team1.score > s1.team2.score ? player1.won + 1 : player1.won;
      player1.lost =
        s1.team1.score < s1.team2.score ? player1.lost + 1 : player1.lost;
      player1.draw =
        s1.team1.score === s1.team2.score ? player1.draw + 1 : player1.draw;
      player1.goal = player1.goal + s1.team1.score;
      player1.goalAgainst = player1.goalAgainst + s1.team2.score;
      player1.points =
        s1.team1.score > s1.team2.score
          ? player1.points + 3
          : s1.team1.score === s1.team2.score
          ? player1.points + 1
          : player1.points;
    }
    if (player2) {
      player2.played = player2.played + 1;
      player2.won =
        s1.team2.score > s1.team1.score ? player2.won + 1 : player2.won;
      player2.lost =
        s1.team2.score < s1.team1.score ? player2.lost + 1 : player2.lost;
      player2.draw =
        s1.team2.score === s1.team1.score ? player2.draw + 1 : player2.draw;
      player2.goal = player2.goal + s1.team2.score;
      player2.goalAgainst = player2.goalAgainst + s1.team1.score;
      player2.points =
        s1.team2.score > s1.team1.score
          ? player2.points + 3
          : s1.team1.score === s1.team2.score
          ? player2.points + 1
          : player2.points;
    }
    this.setState(s1);
  };

  render() {
    let { allMatches, viewIndex, pointArr, team1, team2 } = this.state;
    let teams = pointArr.map((ele) => ele.name);
    console.log(teams);
    return (
      <React.Fragment>
        <div className="container">
          <NavBar Count={allMatches.length} />
          <>
            <button
              className="btn btn-primary m-3 "
              onClick={() => this.handleView(1)}
            >
              All Matches
            </button>{" "}
            <button
              className="btn btn-primary mx-3"
              onClick={() => this.handleView(2)}
            >
              Points Table
            </button>{" "}
            <button
              className="btn btn-primary mx-3"
              onClick={() => this.handleView(3)}
            >
              New Matches{" "}
            </button>{" "}
          </>
          {viewIndex === 1 ? (
            <>
              {allMatches.length === 0 ? (
                <h2 className="text-center">There are no matches</h2>
              ) : (
                <>
                  <h2 className="text-center">Results of the matches so far</h2>
                  <div className="row border bg-dark text-white">
                    <div className="col-3">Team1 </div>
                    <div className="col-3">Team2 </div>
                    <div className="col-3">Score </div>
                    <div className="col-3">Result </div>
                  </div>

                  {allMatches.map((m1) => {
                    let result =
                      m1.score1 === m1.score2
                        ? "Match Drawn"
                        : m1.score1 > m1.score2
                        ? m1.team1 + "Won"
                        : m1.team2 + "Won";
                    return (
                      <div className="row">
                        <div className="col-3">{m1.team1} </div>
                        <div className="col-3">{m1.team2} </div>
                        <div className="col-3">
                          {m1.score1 + "-" + m1.score2}{" "}
                        </div>
                        <div className="col-3">{result} </div>
                      </div>
                    );
                  })}
                </>
              )}
            </>
          ) : viewIndex === 2 ? (
            <>
              <h2 className="text-center">Points Table</h2>
              <div className="row border text-center bg-dark text-white">
                <div className="col-2">Team </div>
                <div className="col-1">played </div>
                <div className="col-1">Won </div>
                <div className="col-1">Lost </div>
                <div className="col-1">Drawn</div>
                <div className="col-2">Goals For </div>
                <div className="col-3">Goals Against </div>
                <div className="col-1">Points </div>
              </div>
              {pointArr.map((p1) => (
                <div className="row border text-center">
                  <div className="col-2">{p1.name} </div>
                  <div className="col-1">{p1.played} </div>
                  <div className="col-1">{p1.won} </div>
                  <div className="col-1">{p1.lost} </div>
                  <div className="col-1">{p1.draw} </div>
                  <div className="col-2">{p1.goal} </div>
                  <div className="col-3">{p1.goalAgainst} </div>
                  <div className="col-1">{p1.points} </div>
                </div>
              ))}
            </>
          ) : viewIndex === 3 ? (
            <>
              <h2 className="text-center">
                {team1.name ? "Team 1 : " + team1.name : "Choose Team 1"}
              </h2>
              <div className="row">
                {teams.map((t1, index) => (
                  <div className="col">
                    <button
                      className="btn btn-warning m-3"
                      onClick={() => this.handleTeam1(t1)}
                    >
                      {t1}{" "}
                    </button>
                  </div>
                ))}
              </div>
              <h2 className="text-center">
                {team2.name ? "Team 2 : " + team2.name : "Choose Team 2"}
              </h2>
              <div className="row">
                {teams.map((t1, index) => (
                  <div className="col">
                    <button
                      className="btn btn-warning m-3"
                      onClick={() => this.handleTeam2(t1)}
                    >
                      {t1}{" "}
                    </button>
                  </div>
                ))}
              </div>
              <div className="col-12 text-center">
                <button
                  className="btn btn-secondary m-3"
                  onClick={() => this.handleStatMatch()}
                >
                  Start Matche{" "}
                </button>
              </div>
            </>
          ) : viewIndex === 4 ? (
            <>
              <h2 className="text-center">Welcome to an exciting match</h2>
              <div className="row text-center m-3">
                <div className="col-4">
                  <h3>{team1.name} </h3>
                </div>
                <div className="col-4">
                  <h3>{team1.score + "-" + team2.score} </h3>
                </div>
                <div className="col-4">
                  <h3>{team2.name}</h3>{" "}
                </div>
              </div>
              <div className="row text-center m-3">
                <div className="col-4">
                  {" "}
                  <button
                    className="btn btn-warning "
                    onClick={() => this.handleScore(`${team1.name}`)}
                  >
                    Goal Scored{" "}
                  </button>
                </div>
                <div className="col-4"></div>
                <div className="col-4">
                  {" "}
                  <button
                    className="btn btn-warning "
                    onClick={() => this.handleScore(`${team2.name}`)}
                  >
                    Goal Scored{" "}
                  </button>
                </div>
              </div>
              <div className="col-12 text-center">
                <button
                  className="btn btn-warning m-3"
                  onClick={() => this.handleMatchOver()}
                >
                  Matche Over{" "}
                </button>
              </div>
            </>
          ) : (
            ""
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default FootballMainCompo;
