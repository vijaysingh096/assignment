/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
class AddNewProdForm extends Component {
  state = {
    product1: this.props.product1,
    allOptions: this.props.allOptions,
    editIndex: this.props.editIndex,
    errors: {},
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    input.type === "checkbox"
      ? (s1.product1[input.name] = input.checked)
      : (s1.product1[input.name] = input.value);
    this.handleValidate(e);
    this.setState(s1);
  };
  handleValidate = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    switch (input.name) {
      case "code":
        s1.errors.code = this.validateCode(input.value);
        break;
      case "price":
        s1.errors.price = this.validatePrice(input.value);
        break;

      default:
        break;
    }
    this.setState(s1);
  };
  validateAll = () => {
    let { code, price, category, brand } = this.state.product1;
    let errors = {};
    errors.code = this.validateCode(code);
    errors.price = this.validatePrice(price);
    errors.category = this.validateCategory(category);
    errors.brand = this.validateBrand(brand);
    return errors;
  };
  validateCode = (code) => (!code ? "Product Code is required" : "");

  validatePrice = (Price) => (!Price ? "Price  is required" : "");
  validateCategory = (category) => (!category ? "Select Category" : "");
  validateBrand = (brand) => (!brand ? "Select brand" : "");

  isValid = (errors) => {
    let keys = Object.keys(errors);
    let countError = keys.reduce(
      (acc, curr) => (errors[curr] ? acc + 1 : acc),
      0
    );
    return countError === 0;
  };

  handleSubmit = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let errors = this.validateAll();
    if (this.isValid(errors)) this.props.onSubmit(this.state.product1);
    else {
      s1.errors = errors;
      this.setState(s1);
    }

    let { code, price, category, brand } = s1.product1;
    if (!code) {
      return window.alert("Product Code is required");
    }
    if (!price) {
      return window.alert("Price  is required");
    }
    if (!category) {
      return window.alert("Select category");
    }
    if (!brand) {
      return window.alert("Select brand");
    }
  };

  handleToHomePage = () => {
    this.props.onHomePage();
  };

  makeDropdown = (arr, value, name, label) => {
    return (
      <div className="form-group my-1">
        <select
          className="form-control"
          name={name}
          value={value}
          onChange={this.handleChange}
        >
          <option value="">{label}</option>
          {arr.map((opt) => (
            <option>{opt}</option>
          ))}
        </select>
      </div>
    );
  };
  makeRadios = (arr, values, name, lable) => {
    return (
      <React.Fragment>
        <lable className="form-check-lable font-weight-bold">
          <b> {lable}</b>{" "}
        </lable>
        <br />
        {arr.map((opt, index) => (
          <div className="form-check form-check-inline m-2 " key={index}>
            <input
              className="form-check-input "
              value={opt.value}
              type="radio"
              name={name}
              checked={values === opt.value || false}
              onChange={this.handleChange}
            />
            <lable className="form-check-lable">{opt.display} </lable>
          </div>
        ))}
        <br />
      </React.Fragment>
    );
  };
  render() {
    let { product1, allOptions, editIndex, errors } = this.state;
    let [Brands, categArr] = allOptions;
    // console.log(Brands, categArr)
    let {
      code = "",
      price = "",
      brand = "",
      category = "Food",
      specialOffer = false,
      limitedStock = false,
      quantity = 0,
    } = product1;
    let brandArr =
      product1.category === "Food"
        ? Brands.Food
        : product1.category === "Personal Care"
        ? Brands.PersonalCare
        : product1.category === "Apparel"
        ? Brands.Apparel
        : [];

    console.log(brandArr);

    return (
      <React.Fragment>
        <div class="form-group">
          <label>Product Code</label>
          <input
            type="text"
            class="form-control"
            id="code"
            name="code"
            readOnly={editIndex >= 0 ? true : false}
            value={code}
            placeholder="Enter Product Code "
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.code ? (
            <span className="form-control text-danger alert-danger">
              {errors.code}
            </span>
          ) : (
            ""
          )}
        </div>
        <div class="form-group">
          <label>Price</label>
          <input
            type="number"
            class="form-control"
            id="price"
            name="price"
            value={price}
            placeholder="Enter Product Price"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.price ? (
            <span className="form-control text-danger alert-danger">
              {errors.price}
            </span>
          ) : (
            ""
          )}
        </div>
        {this.makeRadios(categArr, category, "category", "Select Category")}

        {this.makeDropdown(brandArr, brand, "brand", "Select Brand")}
        <lable className="form-check-lable font-weight-bold">
          <b> Choose other info about the product</b>{" "}
        </lable>

        <div className="form-check">
          <input
            className="form-check-input"
            value={specialOffer}
            type="checkbox"
            name="specialOffer"
            checked={specialOffer || false}
            onChange={this.handleChange}
          />
          <lable className="form-check-lable">Special Offer </lable>
        </div>
        <div className="form-check">
          <input
            className="form-check-input"
            value={limitedStock}
            type="checkbox"
            name="limitedStock"
            checked={limitedStock || false}
            onChange={this.handleChange}
          />
          <lable className="form-check-lable">Special Offer </lable>
        </div>

        <button className="btn btn-primary" onClick={this.handleSubmit}>
          {editIndex >= 0 ? "Edit Product" : "Add Product"}
        </button>
        <br />
        <button
          className="btn btn-primary my-4"
          onClick={this.handleToHomePage}
        >
          Go To Home Page
        </button>
      </React.Fragment>
    );
  }
}
export default AddNewProdForm;
