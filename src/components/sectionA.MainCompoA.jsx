/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import NavBarA from "./sectionA.NavbarA";
class MainCompoA extends Component {
  state = {
    Products: [
      {
        code: "PEP221",
        prod: "Pepsi",
        price: 12,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "COK113",
        prod: "Coca Cola",
        price: 18,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "MIR646",
        prod: "Mirinda",
        price: 15,
        instock: "No",
        category: "Beverages",
      },
      {
        code: "SLI874",
        prod: "Slice",
        price: 22,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "MIN654",
        prod: "Minute Maid",
        price: 25,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "APP652",
        prod: "Appy",
        price: 10,
        instock: "No",
        category: "Beverages",
      },
      {
        code: "FRO085",
        prod: "Frooti",
        price: 30,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "REA546",
        prod: "Real",
        price: 24,
        instock: "No",
        category: "Beverages",
      },
      {
        code: "DM5461",
        prod: "Dairy Milk",
        price: 40,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "KK6546",
        prod: "Kitkat",
        price: 15,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "PER5436",
        prod: "Perk",
        price: 8,
        instock: "No",
        category: "Chocolates",
      },
      {
        code: "FST241",
        prod: "5 Star",
        price: 25,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "NUT553",
        prod: "Nutties",
        price: 18,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "GEM006",
        prod: "Gems",
        price: 8,
        instock: "No",
        category: "Chocolates",
      },
      {
        code: "GD2991",
        prod: "Good Day",
        price: 25,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "PAG542",
        prod: "Parle G",
        price: 5,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "MON119",
        prod: "Monaco",
        price: 7,
        instock: "No",
        category: "Biscuits",
      },
      {
        code: "BOU291",
        prod: "Bourbon",
        price: 22,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "MAR951",
        prod: "MarieGold",
        price: 15,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "ORE188",
        prod: "Oreo",
        price: 30,
        instock: "No",
        category: "Biscuits",
      },
    ],
    categoryArr: ["Beverages", "Chocolates", "Biscuits"],
    stockArr: ["Yes", "No"],
    priceRangeArr: ["<10", "10-20", ">20"],
    billDetail: [],
    ddSelValue: {},
    // fltrIndex: -1,
    // showFilterData: [],
    x: -1,
    viewIndex: -1,
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    s1.ddSelValue[input.name] = input.value;
    this.setState(s1);
  };
  handleNewBill = () => {
    let s1 = { ...this.state };
    s1.viewIndex = 1;
    s1.billDetail=[]
    this.setState(s1);
  };
  filterArr = (arr, selVals) => {
    let { category = "", stock = "", price = "" } = selVals;
    let Product1 = category
      ? arr.filter((p1) => p1.category === category)
      : arr;
    let Product2 = stock
      ? Product1.filter((p1) => p1.instock === stock)
      : Product1;
    let Product3 =
      price === "<10"
        ? Product2.filter((p1) => p1.price < 10)
        : price === "10-20"
        ? Product2.filter((p1) => p1.price >= 10 && p1.price <= 20)
        : price === ">20"
        ? Product2.filter((p1) => p1.price > 20)
        : Product2;
    return Product3;
  };
  sortProd = (colNo) => {
    console.log(colNo);
    let s1 = { ...this.state };
    switch (colNo) {
      case 0:
        s1.Products.sort((prod1, prod2) =>
          prod1.code.localeCompare(prod2.code)
        );
        s1.x = 0;
        break;
      case 1:
        s1.Products.sort((prod1, prod2) =>
          prod1.prod.localeCompare(prod2.prod)
        );
        s1.x = 1;
        break;
      case 2:
        s1.Products.sort((prod1, prod2) =>
          prod1.category.localeCompare(prod2.category)
        );
        s1.x = 2;
        break;
      case 3:
        s1.Products.sort((prod1, prod2) => +prod1.price - +prod2.price);
        s1.x = 3;
        break;
      case 4:
        s1.Products.sort((prod1, prod2) =>
          prod1.instock.localeCompare(prod2.instock)
        );
        s1.x = 4;
        break;
      default:
        return s1.Products;
    }
    this.setState(s1);
  };
  addToBill = (prodName) => {
    let s1 = { ...this.state };
    let p1 = s1.billDetail.find((pro) => pro.prod === prodName);
    if (p1) {
      p1.quntity++;
      p1.value = p1.value + p1.price;
    } else {
      let product1 = s1.Products.find((ele) => ele.prod === prodName);
      let newArr = {
        code: product1.code,
        prod: product1.prod,
        price: product1.price,
        quntity: 0,
        value: 0,
      };
      newArr.quntity = 1;
      newArr.value = newArr.quntity * newArr.price;
      s1.billDetail.push(newArr);
    }
    this.setState(s1);
  };
  addOne = (prodName) => {
    let s1 = { ...this.state };
    let p1 = s1.billDetail.find((pro) => pro.prod === prodName);
    if (p1) {
      p1.quntity++;
      p1.value = p1.value + p1.price;
    }
    this.setState(s1);
  };
  subtOne = (prodName) => {
    let s1 = { ...this.state };
    let p1 = s1.billDetail.find((pro) => pro.prod === prodName);

    if (p1.quntity > 0) {
      p1.quntity--;
      p1.value = p1.value - p1.price;
    }
    if (p1.quntity <= 0) {
      let index = s1.billDetail.findIndex((ele) => ele.prod === p1.prod);
      s1.billDetail.splice(index, 1);
    }

    this.setState(s1);
  };
  cancle = (index) => {
    let s1 = { ...this.state };
    s1.billDetail.splice(index, 1);
    this.setState(s1);
  };

  closeBill = () => {
    let s1 = { ...this.state };
    window.alert("Closing the current bill ");
    s1.billDetail = [];
    this.setState(s1);
  };
  render() {
    let {
      Products,
      viewIndex,
      billDetail,
      categoryArr,
      stockArr,
      priceRangeArr,
      ddSelValue,
      x,
    } = this.state;
    let countQuntity = billDetail.reduce((acc, curr) => acc + curr.quntity, 0);
    let countAmount = billDetail.reduce((acc, curr) => acc + curr.value, 0);
    let { category = "", stock = "", price = "" } = ddSelValue;
    let showTableArr = this.filterArr(Products, ddSelValue);
    let codX = x === 0 ? "Code (x)" : "Code";
    let prodX = x === 1 ? "Product(x)" : "Product";
    let catX = x === 2 ? "Category(x)" : "Category";
    let priceX = x === 3 ? "Price(x)" : "Price ";
    let stockX = x === 4 ? "In Stock(x)" : "In Stock";
    return (
      <React.Fragment>
        <div className="container">
          <NavBarA onNewBill={this.handleNewBill} />
          {viewIndex < 0 ? (
            ""
          ) : (
            <>
              <h2>Detail of Current Bill</h2>
              <p>
                {" "}
                Item: <b> {billDetail.length} </b>, Quantity:{" "}
                <b> {countQuntity} </b>, Amount: <b> {countAmount}</b>
              </p>
              {billDetail.length > 0 ? (
                <>
                  {billDetail.map((prod1, index) => {
                    let { code, prod, price, quntity, value } = prod1;
                    return (
                      <div className="row border " key={code}>
                        <div className="col-6 ">
                          
                            {code}, {prod}, Price : {price}, Quantity: {quntity}
                            , Value: {value}{" "}
                          
                        </div>

                        <div className="col-6 bg-light ">
                          <button
                            className="btn btn-success  "
                            onClick={() => this.addOne(prod)}
                          >
                            +
                          </button>
                          <button
                            className="btn btn-warning mx-1"
                            onClick={() => this.subtOne(prod)}
                          >
                            -
                          </button>
                          <button
                            className="btn btn-danger "
                            onClick={() => this.cancle(index)}
                          >
                            x
                          </button>
                        </div>
                      </div>
                    );
                  })}
                  <button
                    class="btn btn-primary btn-sm"
                    onClick={() => this.closeBill()}
                  >
                    Close Bill
                  </button>
                </>
              ) : (
                ""
              )}
              <hr />
              <h2 className="text-center">Products List</h2>
              <hr />
              <div className="row">
                <div className="col-3">
                  <h3>Filter Products By : </h3>{" "}
                </div>
                <div className="col-3">
                  {" "}
                  {this.makeDropdown(
                    categoryArr,
                    category,
                    "category",
                    "Select Category"
                  )}{" "}
                </div>
                <div className="col-3">
                  {this.makeDropdown(stockArr, stock, "stock", "Select Stock")}{" "}
                </div>
                <div className="col-3">
                  {" "}
                  {this.makeDropdown(
                    priceRangeArr,
                    price,
                    "price",
                    "Select Price Range"
                  )}
                </div>
              </div>
              <div class="row bg-dark text-white">
                <div class="col-2" onClick={() => this.sortProd(0)}>
                  {codX}
                </div>
                <div class="col-2" onClick={() => this.sortProd(1)}>
                  {prodX}{" "}
                </div>
                <div class="col-2" onClick={() => this.sortProd(2)}>
                  {catX}
                </div>
                <div class="col-1" onClick={() => this.sortProd(3)}>
                  {priceX}
                </div>
                <div class="col-2" onClick={() => this.sortProd(4)}>
                  {stockX}
                </div>
                <div class="col-3"></div>
              </div>
              {showTableArr.map((prod1, index) => {
                let { code, prod, category, price, instock } = prod1;
                return (
                  <div class="row border" key={code}>
                    <div class="col-2"> {code}</div>
                    <div class="col-2">{prod}</div>
                    <div class="col-2">{category}</div>
                    <div class="col-1">{price}</div>
                    <div class="col-2"> {instock}</div>
                    <div class="col-3 ">
                      <button
                        class="btn btn-secondary btn-sm"
                        onClick={() => this.addToBill(prod)}
                      >
                        Add To Bill
                      </button>
                    </div>
                  </div>
                );
              })}
            </>
          )}
        </div>
      </React.Fragment>
    );
  }
  makeDropdown = (arr, value, name, label) => {
    return (
      <div className="form-group my-1">
        <select
          className="form-control"
          name={name}
          value={value}
          onChange={this.handleChange}
        >
          <option value="">{label}</option>
          {arr.map((opt, index) => (
            <option key={index}>{opt}</option>
          ))}
        </select>
      </div>
    );
  };
}
export default MainCompoA;
