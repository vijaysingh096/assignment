/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
class NavBarAssign7 extends Component {
  render() {
    let { pCount, qCount, vCount } = this.props;

    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
          <a className="navbar-brand" href="#">
            ProductStoreSys
          </a>
          <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Products
                  <span className="badge badge-pill bg-secondary mx-2 badge-secondary">
                    {pCount}
                  </span>
                </a>
              </li>

              <li className="nav-item">
                <a className="nav-link" href="#">
                  Quantity
                  <span className="badge badge-pill bg-secondary mx-2 badge-secondary">
                    {qCount}
                  </span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Value
                  <span className="badge badge-pill bg-secondary mx-2 badge-secondary">
                    {vCount}
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}
export default NavBarAssign7;
