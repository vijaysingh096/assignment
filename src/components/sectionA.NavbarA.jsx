/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
class NavBarA extends Component {
  handleViewBill = () => {
    this.props.onNewBill();
  };
  render() {
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
          <a className="navbar-brand" href="#">
            BillingSystem
          </a>
          <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item" onClick={() => this.handleViewBill()}>
                <a className="nav-link" href="#">
                  New Bill
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}
export default NavBarA;
