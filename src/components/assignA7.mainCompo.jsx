import React, { Component } from "react";
import NavBarAssign7 from "./assignA7.Navbar";
import AddNewProdForm from "./assignA7.ShowForm";
import StockForm from "./assignA7.StockForm";
class MainCompoAssign7 extends Component {
  state = {
    Brands: {
      Food: [
        "Nestle",
        "Haldiram",
        "Pepsi",
        "Coca Cola",
        "Britannia",
        "Cadburys",
      ],

      PersonalCare: ["P&G", "Colgate", "Parachute", "Gillete", "Dove"],
      Apparel: ["Levis", "Van Heusen", "Manyavaar", "Zara"],
    },

    Products: [
      {
        code: "PEP1253",
        price: 20,
        brand: "Pepsi",
        category: "Food",
        specialOffer: false,
        limitedStock: false,
        quantity: 25,
      },
      {
        code: "MAGG021",
        price: 25,
        brand: "Nestle",
        category: "Food",
        specialOffer: true,
        limitedStock: true,
        quantity: 10,
      },
      {
        code: "LEV501",
        price: 1000,
        brand: "Levis",
        category: "Apparel",
        specialOffer: true,
        limitedStock: true,
        quantity: 3,
      },
      {
        code: "CLG281",
        price: 60,
        brand: "Colgate",
        category: "Personal Care",
        specialOffer: true,
        limitedStock: true,
        quantity: 5,
      },
      {
        code: "MAGG451",
        price: 25,
        brand: "Nestle",
        category: "Food",
        specialOffer: true,
        limitedStock: true,
        quantity: 0,
      },
      {
        code: "PAR250",
        price: 40,
        brand: "Parachute",
        category: "Personal Care",
        specialOffer: true,
        limitedStock: true,
        quantity: 5,
      },
    ],
    viewIndex: -1,
    editIndex: -1,
    categArr: [
      { display: "Food", value: "Food" },
      { display: "Personal Care", value: "Personal Care" },
      { display: "Apparel", value: "Apparel" },
    ],
  };
  handleAddNewProd = () => {
    let s1 = { ...this.state };
    s1.viewIndex = 1;
    this.setState(s1);
  };
  handleRecevieStock = () => {
    let s1 = { ...this.state };
    s1.viewIndex = 2;
    this.setState(s1);
  };
  handleSubmit = (prod) => {
    let s1 = { ...this.state };
    s1.editIndex >= 0
      ? (s1.Products[s1.editIndex] = prod)
      : s1.Products.push(prod);
    s1.viewIndex = -1;
    this.setState(s1);
  };
  handleViewHome = () => {
    let s1 = { ...this.state };
    s1.viewIndex = -1;
    this.setState(s1);
  };
  handleEditDetails = (index) => {
    let s1 = { ...this.state };
    s1.editIndex = index;
    s1.viewIndex = 1;
    this.setState(s1);
  };
  handleSubmitStock = (stock) => {
    let s1 = { ...this.state };
    let prod = s1.Products.find((p1) => p1.code === stock.code);
    if (prod) {
      prod.quantity = stock.quantity;
    }
    s1.viewIndex = -1;
    this.setState(s1);
  };
  render() {
    let { Products, Brands, viewIndex, editIndex, categArr } = this.state;
    let allQnty = Products.reduce(
      (acc, curr) => (acc = acc + curr.quantity),
      0
    );
    let allValues = Products.reduce(
      (acc, curr) => (acc = acc + curr.quantity * curr.price),
      0
    );
    let allOptions = [Brands, categArr];
    let product1 = {
      code: "",
      price: "",
      brand: "",
      category: "",
      specialOffer: false,
      limitedStock: false,
      quantity: 0,
    };
    let stock1 = { code: "", quantity: "", year: "", month: "", date: "" };
    let codeArr = Products.map((p1) => p1.code);
    return (
      <React.Fragment>
        <div className="container">
          <NavBarAssign7
            pCount={Products.length}
            qCount={allQnty}
            vCount={allValues}
          />
          {viewIndex < 0 ? (
            <>
              <div className="row">
                {Products.map((p1, index) => (
                  <>
                    <div className="col-3 border bg-light text-center">
                      <h6>Code : {p1.code} </h6>
                      Brand : {p1.brand}
                      <br />
                      Category : {p1.category}
                      <br />
                      Price : {p1.price}
                      <br />
                      Quantity : {p1.quantity}
                      <br />
                      Special Offer : {p1.specialOffer ? "Yes" : "No"}
                      <br />
                      Limited Stock : {p1.limitedStock ? "Yes" : "No"}
                      <br />
                      <button
                        className="btn btn-warning m-3"
                        onClick={() => this.handleEditDetails(index)}
                      >
                        {" "}
                        Edit Details
                      </button>
                    </div>
                  </>
                ))}
              </div>
              <button
                className="btn btn-primary m-3"
                onClick={() => this.handleAddNewProd()}
              >
                {" "}
                Add New Product
              </button>
              <button
                className="btn btn-primary m-3"
                onClick={() => this.handleRecevieStock()}
              >
                {" "}
                Receive Stock
              </button>
            </>
          ) : viewIndex === 1 ? (
            <AddNewProdForm
              product1={editIndex >= 0 ? Products[editIndex] : product1}
              allOptions={allOptions}
              editIndex={editIndex}
              onHomePage={this.handleViewHome}
              onSubmit={this.handleSubmit}
            />
          ) : viewIndex === 2 ? (
            <>
              <StockForm
                codeArr={codeArr}
                stock1={stock1}
                onHomePage={this.handleViewHome}
                onSubmitStock={this.handleSubmitStock}
              />
            </>
          ) : (
            ""
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default MainCompoAssign7;
