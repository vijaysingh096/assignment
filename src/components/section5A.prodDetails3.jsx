import React, { Component } from "react";
class ProductDetails3 extends Component {
  state = {
    productsArr: [
      { product: "Pepsi", sales: [2, 5, 8, 10, 5] },
      { product: "Coke", sales: [3, 6, 5, 4, 11, 5] },
      { product: "5Star", sales: [10, 14, 22] },
      { product: "Maggi", sales: [3, 3, 3, 3, 3] },
      { product: "Perk", sales: [1, 2, 1, 2, 1, 2] },
      { product: "Bingo", sales: [0, 1, 0, 3, 2, 6] },
      { product: "Gems", sales: [3, 3, 1, 1] },
    ],
    showTable: -1,
    detailIndex: -1,
  };

  sortTable = (n1) => {
    let s1 = { ...this.state };
    s1.showTable = n1;
    this.setState(s1);
  };

  handleDetail = (index) => {
    let s1 = { ...this.state };
    s1.detailIndex = index;
    this.setState(s1);
  };

  render() {
    let { productsArr, showTable, detailIndex } = this.state;
    let showData =
      showTable === 0
        ? productsArr.sort((p1, p2) => p1.product.localeCompare(p2.product))
        : showTable === 1
        ? productsArr.sort(
            (p1, p2) =>
              p1.sales.reduce((a1, c1) => a1 + c1) -
              p2.sales.reduce((a2, c2) => a2 + c2)
          )
        : showTable === 2
        ? productsArr.sort(
            (p1, p2) =>
              p2.sales.reduce((a, c) => a + c) -
              p1.sales.reduce((a, c) => a + c)
          )
        : productsArr;
    return (
      <React.Fragment>
        <div className="container">
          <button
            className="btn btn-primary mx-5"
            onClick={() => this.sortTable(0)}
          >
            Sort by Product
          </button>
          <button
            className="btn btn-primary mx-5 "
            onClick={() => this.sortTable(1)}
          >
            Total Sales Asc
          </button>
          <button
            className="btn btn-primary mx-5"
            onClick={() => this.sortTable(2)}
          >
            Total Sales Desc
          </button>
          <div className="row border bg-black text-white">
            <div className="col-4">Product</div>
            <div className="col-4">Total Sales</div>
            <div className="col-4"></div>
          </div>
          {showData.map((p1, index) => (
            <div className="row border">
              <div className="col-4">{p1.product}</div>
              <div className="col-4">
                {p1.sales.reduce((a, c) => a + c, 0)}{" "}
              </div>
              <div className="col-4">
                <button
                  className="btn btn-warning btn-sm"
                  onClick={() => this.handleDetail(index)}
                >
                  Details
                </button>
              </div>
            </div>
          ))}
          {detailIndex >= 0 ? (
            <>
              {" "}
              <p>
                Product :<b> {productsArr[detailIndex].product}</b>
              </p>
              <b>Sales</b>
              <ul>
                {productsArr[detailIndex].sales.map((s1) => (
                  <li>{s1}</li>
                ))}
              </ul>
            </>
          ) : (
            ""
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default ProductDetails3;
