/* eslint-disable jsx-a11y/img-redundant-alt */
import React,{Component} from "react";
import Cart from "./sectionB.Cart";

class OtherItems extends Component{
    state = {
        cart: this.props.cart,
        otherItemArr: this.props.otherItemArr,
        sizesArr: this.props.sizesArr,
        crustsArr: this.props.crustsArr,
      };
    
      addOtherItemToCart = (index, itemName) => {
        let s1 = { ...this.state };
        console.log( itemName);
    
        let d1 = s1.cart.find((dish) => dish.name === itemName);
        if (d1) {
          d1.quntity++;
        } else {
          let item1 = s1.otherItemArr.find((ele) => ele.name === itemName);
          let newArr = {
            image: item1.image,
            name: item1.name,
            desc: item1.desc,
            quntity: 0,
        };
        newArr.quntity = 1;
        this.props.addCart(newArr);
    }
        this.setState(s1);
      };
    
      handleAdd = (Name) => {
        this.props.addOne(Name);
      };
      handleSubt = (Name) => {
        this.props.subtOne(Name);
      };
      render() {
        let { cart, otherItemArr } = this.state;
    
        return (
          <React.Fragment>
            <div className="row">
              <div className="col-8">
                <div className="row ">
                  {otherItemArr.map((pizza1, index) => {
                    let { image, name, desc } = pizza1;
                    let cartItem = cart.find((item) => item.name === name);
    
                    console.log(image);
                    return (
                      <div className="col-6">
                        <div
                          className="card text-center"
                          style={{ width: 90 + "%" }}
                        >
                          <img
                            className="card-img-top align-items-center "
                            style={{ width: 90 + "%" }}
                            src={image}
                            alt="Card image cap"
                          />
                          <div className="card-body">
                            <h5 className="card-title text-center"> {name}</h5>
                            <p className="card-text">{desc}</p>
                            {cartItem ? (
                              <>
                                <button
                                  className="btn btn-danger btn-sm "
                                  onClick={() => this.handleSubt(cartItem.name)}
                                >
                                  -
                                </button>
                                <button
                                  className="btn btn-secondary mx-2 btn-sm"
                                  disabled={true}
                                >
                                  {cartItem.quntity}
                                </button>
                                <button
                                  className="btn btn-success btn-sm "
                                  onClick={() => this.handleAdd(cartItem.name)}
                                >
                                  +
                                </button>
                              </>
                            ) : (
                              <>
                                <button
                                  className="btn btn-primary "
                                  onClick={() => this.addOtherItemToCart(index, name)}
                                >
                                  Add To Cart
                                </button>
                              </>
                            )}
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
              <div className="col-4 border">
                <Cart
                  cart={cart}
                  addOne={this.props.addOne}
                  subtOne={this.props.subtOne}
                />
              </div>
            </div>
          </React.Fragment>
        );
      }
}
export default OtherItems